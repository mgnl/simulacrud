# Simulacrud - Simple library for PHP

## Instalation

Install the latest version with

```bash
$ composer require mgnl/simulacrud
```

## License

Simulacrud is licensed under the MIT License - see the `LICENSE` file for details
