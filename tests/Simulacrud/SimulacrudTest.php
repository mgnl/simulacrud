<?php
declare(strict_types=1);

/**
 * This file is part of the Simulacrud package.
 *
 * (c) Mgnl Team <mgnl.library@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Simulacrud;

use PHPUnit\Framework\TestCase;

final class SimulacrudTest extends TestCase
{

    /**
     * @test
     */
    public function is_correct_instance_type()
    {
        $simulacrud = new Simulacrud();

        $this->assertInstanceOf(Simulacrud::class, $simulacrud);
    }
}
